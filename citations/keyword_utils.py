from citations import __keywords
from typing import List, Tuple, Dict, Set, Optional, Union
import json


def get_keywords_data() -> Dict[
    str, Union[Set[str], Dict[str, Set[str]], List[Tuple[str, bool]]]
]:
    """
    :return: The keywords used by the package as a Python Dictionary.
    """
    return __keywords.get()


def get_keywords() -> str:
    """
    :return: The keywords used by the package encoded as a json string.
    """
    data = get_keywords_data()
    data["fillers"] = list(data["fillers"])
    data["join_words"] = list(data["join_words"])
    for key in data["keys"]:
        data["keys"][key] = list(data["keys"][key])
    return json.dumps(data)


def set_keywords_data(
    data: Union[
        str, Dict[str, Union[Set[str], Dict[str, Set[str]], List[Tuple[str, bool]]]]
    ]
) -> bool:
    """
    Set the keywords used by the package to the provided ones.

    :param data: The provided keywords.
        Either as json encoded string or as a dictionary
        like the one returned by :func:`get_keywords`.
    :return: Whether setting the keywords succeeded.
    """
    global __keywords
    try:
        if type(data) is str:
            data = json.loads(data)
    except json.JSONDecodeError as err:
        return False
    # I need to check the data here.
    __keywords.set(data)
    return True


def set_keywords(data: str) -> bool:
    data = json.loads(data)
    data["fillers"] = set(data["fillers"])
    data["join_words"] = set(data["join_words"])
    for key in data["keys"]:
        data["keys"][key] = set(data["keys"][key])
    return set_keywords_data(data)
