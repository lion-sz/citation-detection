from citations.keywords import Keywords

# If run as a module, I need to use the default keywords,
# otherwise the Keywords class checks for an existing keywords file.
__keywords: Keywords
if __name__ == "citations":
    __keywords = Keywords(default=True)
else:
    __keywords = Keywords()

from citations.keyword_utils import set_keywords, set_keywords_data, get_keywords, get_keywords_data
from citations.interface import replace, replace_html, extract
