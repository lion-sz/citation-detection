from typing import List, Tuple, Dict, Set, Optional, Union
import pathlib


class Keywords:
    """
    This class is one point to store all the relevant keywords in a way
    such that I can easily export and save it as a json file.
    This also allows me to integrate the code with the viewer app,
    which allows the user to update the keywords from the json file.
    """

    citation_struct: List[Tuple[str, bool]]
    citation_keys: Dict[str, Set[str]]
    fillers: Set[str]
    join_words: Set[str]
    max_irrelevant: int
    path: Optional[pathlib.Path]

    def __init__(self, default: bool = False, path: Optional[pathlib.Path] = None):
        if default:
            self.path = None
            self.set_defaults()
        else:
            # try to read in the file
            self.path = path if path is not None else pathlib.Path("keywords.json")
            if self.path.exists():
                with self.path.open() as ifile:
                    self.load(ifile.read())
            else:
                self.set_defaults()

    def __del__(self):
        if self.path:
            with self.path.open("w") as jfile:
                jfile.write(self.dump())

    def set_defaults(self):
        self.citation_struct = [
            ("paragraph", True),
            ("absatz", True),
            ("satz", True),
            ("buchstabe", False),
        ]
        self.citation_keys = {
            "paragraph": {"art", "art.", "article", "§"},
            "absatz": {"absatz", "abs", "abs."},
            "satz": {"satz", "s"},
            "buchstabe": {"buchstabe", "lit", "lit."},
        }
        self.fillers = {"der"}
        # This parameter specifies how many irrelevant words a citation can have before I stop parsing.
        # 0 means that I stop when I encounter even one.
        self.max_irrelevant = 1
        # These words are used to join two citations, e.g. Art 5 und 7 DSGVO.
        # If I encounter one of these words I allow the following to be any citation element,
        # while a ',' is only allowed for a second value of the same element.
        self.join_words = {"und"}

    def set(
        self,
        data: Dict[str, Union[Set[str], Dict[str, Set[str]], List[Tuple[str, bool]]]],
    ) -> None:
        self.fillers = data["fillers"]
        self.join_words = data["join_words"]
        self.citation_struct = data["citation_struct"]
        self.citation_keys = data["keys"]

    def get(
        self,
    ) -> Dict[str, Union[Set[str], Dict[str, Set[str]], List[Tuple[str, bool]]]]:
        output = {
            "keys": self.citation_keys,
            "fillers": self.fillers,
            "join_words": self.join_words,
            "citation_struct": self.citation_struct,
        }
        return output
