from typing import Tuple, List, Optional, Callable, Union
import copy

from citations.citation import Citation
from citations import __keywords as keywords


def extract_numeral(data: str) -> Optional[int]:
    """
    This function extracts a number from a string.

    :param data: The word from which a number should be extracted.
    :return: The found Number. None if none was found.
    """
    tmp = "".join(filter(lambda x: x.isdigit() or x == "-", data))
    if len(tmp) == 0 or len("".join(filter(str.isdigit, tmp))) == 0:
        return None
    return int(tmp) if int(tmp) > 0 else None


def extract_citation(possible: List[str]) -> Tuple[List[Citation], int]:
    """
    This function extracts a citation from a possible citation from a list of words.
    To reconstruct the sentence from the citations,
    I need to return the position in the string, that the parser stopped at.
    """
    citation_text = [
        possible[0]
    ]  # This list stores the elements that belong to the citation.
    # Now I need to go through this possible citation
    # and try to extract information from it.
    # first the article, which I know to be in a fixed position.
    cits = [Citation.new(possible)]
    cit_pos = 1
    cit_struct_pos = 0
    irrelevant = 0
    cit_struct_pos_before_join: Optional[int] = None
    while cit_pos < len(possible) and irrelevant < keywords.max_irrelevant:
        word = possible[cit_pos]
        # first look for a filler word and simply continue.
        if word in keywords.fillers:
            cit_pos += 1
            citation_text.append(word)
            continue
        # now check if there is a join word and I need to start a new citation.
        if word in keywords.join_words:
            # Write the text so far to the citation and create a new one.
            # There are two possible cases. Either a new citation begins with a new article
            # (for this I need to reset cit_struct_pos to zero, so that I can catch this),
            # or I continue from the current level (Art 1 Absatz 2 und 3 DSGVO).
            # To work with this I need a variable that saves this information.
            cits[-1]["text"] = " ".join(citation_text)
            citation_text: List[str] = []
            cits.append(Citation.new(possible))
            cit_struct_pos_before_join = (
                cit_struct_pos  # I can reset this once I find a new reference.
            )
            cit_struct_pos = 0
            cit_pos += 1
            continue
        # first check If I find a new key.
        found_new = False
        for j in range(len(keywords.citation_struct) - cit_struct_pos):
            if (
                word.lower()
                in keywords.citation_keys[
                    keywords.citation_struct[cit_struct_pos + j][0]
                ]
            ):
                # I've found a new structure part. Increase the structure and reset the value.
                citation_text.append(word)
                cit_struct_pos += j
                cit_pos += 1
                cit_struct_pos_before_join = None
                found_new = True
                break
        if found_new:
            continue
        # look for a value
        # first I look for numeric values.
        if keywords.citation_struct[cit_struct_pos][1]:
            tmp_val = extract_numeral(word)
        else:
            # look for a single letter.
            tmp_val = "".join(filter(str.isalpha, word))
            if len(tmp_val) != 1:
                tmp_val = None
        # Now I need to set the value and check for comma separated continuations.
        # I find these by having two valid values, while the previous word contained a ','.
        # The check for a valid value is done by the set_value function
        if tmp_val is not None:
            # first check If I need to use the last position. If I do, I don't need to check for commas.
            if cit_struct_pos_before_join is not None:
                cits[-1][keywords.citation_struct[cit_struct_pos][0]] = tmp_val
            else:
                citation_text.append(word)
                # check for comma separated stuff.
                if not cits[-1].set_value(
                    keywords.citation_struct[cit_struct_pos][0], tmp_val
                ):
                    if "," in citation_text[-1]:
                        # this is a comma joined citation. Otherwise skip the new value.
                        new_cit = copy.deepcopy(cits[-1])
                        new_cit.set_value(
                            keywords.citation_struct[cit_struct_pos][0],
                            tmp_val,
                            overwrite=True,
                        )
                        cits[-1].text = " ".join(
                            citation_text
                        )  # this text still contains the comma...
                        citation_text.pop()
                        citation_text.append(word)
                        cits.append(new_cit)
        else:
            irrelevant += 1
        cit_pos += 1
    # write the gesetz and to all citations.
    cits[-1].text = " ".join(citation_text)
    end_position = cit_pos - irrelevant
    if end_position >= len(possible):
        print("Warning: end position after last word in possible, resetting")
        end_position = len(possible) - 1
    for cit in cits:
        cit["gesetz"] = possible[end_position]
        cit["text"] = cit["text"] + " " + cit["gesetz"]
    return [cit for cit in cits if cit.is_valid()], end_position


def process(text: str) -> List[Union[str, Citation]]:
    """
    I first transform the input text into a list of sentences
    and then those sentences into a list of words.

    For each word I then check if it contains a spelling of paragraph.
    If it does, I try to extract a citation from the next 12 words.
    Therefore, I impose that all citations have at most 12 words.
    But since citations can be joined,
    I pass the next 50 words on to the extraction function.

    :param text: The text from which citations should be extracted.
    :return: The processed text
    :rtype: List[Union[str, Citation]]
    """
    words = [w.strip() for w in text.split() if w.strip() != ""]
    processed: List[Union[str, Citation]] = []
    # now iterate through the words. I need the position, therefore with i.
    end_pos = 0  # The end of the previous citation.
    for i in range(len(words)):
        if i < end_pos:
            # this word was processed as part of a citation. Skip it.
            continue
        elif words[i].lower() not in keywords.citation_keys["paragraph"]:
            # no citation starts here. continue
            processed.append(words[i])
            continue
        cits, cit_len = extract_citation(
            [words[i + j] for j in range(min(50, len(words) - i))]
        )
        if cits:
            for cit in cits:
                processed.append(cit)
            end_pos = i + cit_len
    return processed
