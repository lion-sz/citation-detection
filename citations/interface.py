from typing import Optional, Callable, List, Union

import bs4

from citations.extraction import process
from citations.citation import Citation


def gen_callable(
    tag: str = "span", tag_style: Optional[str] = None, tag_class: Optional[str] = None
) -> Callable[[Citation], bs4.element.Tag]:
    """
    This function generates a callable to format the citation into an html tag.
    It supports custom tags (the default is a span), the class and style attribute.

    :param tag: The tag to be used. `span` by default.
    :type tag: Optional[str]
    :param tag_style: If set the content is applied as the css style attribute
    :type tag_style: Optional[str]
    :param tag_class: If provided, the tags will be set to this class.
    :type tag_class: Optional[str]
    :return: A callable which formats a citation into a html tag encoded as string.
    :rtype: Callable[[Citation], str]
    """
    tag = bs4.element.Tag(name=tag)
    if tag_class:
        tag['class'] = tag_class
    if tag_style:
        tag['style'] = tag_style
    return lambda cit: tag.append(str(cit))


def create_tag(
    cit: Citation, tag_type: str = "span", tag_style: Optional[str] = None, tag_class: Optional[str] = None
) -> bs4.element.Tag:
    """
    This function creates a bs4 tag from a citation.
    It supports custom tags (the default is a span), the class and style attribute.

    :param cit: The citation element which should be formatted.
    :type cit: Citation
    :param tag_type: The tag to be used. `span` by default.
    :type tag_type: Optional[str]
    :param tag_style: If set the content is applied as the css style attribute
    :type tag_style: Optional[str]
    :param tag_class: If provided, the tags will be set to this class.
    :type tag_class: Optional[str]
    :return: The bs4 citation tag.
    :rtype: bs4.element.Tag
    """
    tag = bs4.element.Tag(name=tag_type)
    if tag_class:
        tag['class'] = tag_class
    if tag_style:
        tag['style'] = tag_style
    tag.append(f" {str(cit)} ")
    return tag


def replace_html(soup: bs4.element.Tag, **kwargs) -> None:
    """
    Correctly parsing citations from html is actually quite complicated.
    I can't simply go through the text of the element, since there might be nested structures.
    If a tags contain other child tags, I need to keep the position of these tags relative to the text
    of the parent tag. Otherwise I might end up shuffling text around.

    To prevent this, I check if the element has any children.
    If the element has, I iterate through all children, search for citations in the text around all elements
    and recursively apply this function to all children with more than 3 words of text
    distributed across any of their children.
    Elements with less than three words of text are simply removed and their text added to the previous text.

    An option to keep (or reconstruct) these small tags should be implemented later.

    :param soup: The bs4 element which I should replace.
    :type soup: bs4.element.Tag
    :param span_style: The css style attribute that should be applied to the citation span.
    :type span_style: Optional[str]
    :param span_class: The class that the span tags should have.
    :type span_class: Optional[str]
    :return: No return since the soup element is changed.
    :rtype: None
    """
    text = soup.text.strip()
    if len(text) == 0:
        return None
    processed_full = process(text)
    cits_total = len(list(filter(lambda x: type(x) is Citation, processed_full)))
    if cits_total == 0:
        # There are no citations anywhere in this tag, I can return.
        return None
    tag_creation = gen_callable(**kwargs)
    content: List[Union[str, bs4.element.Tag]] = []
    prev_text = ''
    for child in soup.children:
        if type(child) is bs4.element.Tag:
            if len(child.text.split()) >= 3:
                replace_html(child, **kwargs)
                if prev_text:
                    content.append(prev_text)
                    prev_text = ''
                content.append(child)
            else:
                prev_text += child.text
        else:
            prev_text += child
    if prev_text:
        content.append(prev_text)
    # Now clear the tag and rebuild it form the contents.
    soup.clear()
    for elem in content:
        if type(elem) is bs4.element.Tag:
            # this is an element, just append it.
            soup.append(elem)
        else:
            processed = process(elem)
            tmp: List[str] = []
            for part in processed:
                if type(part) is str:
                    tmp.append(part)
                elif type(part) is Citation:
                    if tmp:
                        soup.append(' '.join(tmp))
                    soup.append(create_tag(part, **kwargs))
                else:
                    print("False type found")
            if tmp:
                soup.append(' '.join(tmp))


def replace(text: str) -> str:
    """
    This function replaces all citations in the input string with the standardized
    citation format.

    :param text: The input text that should be processed.
    :type text: str
    :return: The input text but with the standardized citations.
    :rtype: str
    """
    processed = process(text)
    res = []
    for elem in processed:
        if isinstance(elem, str):
            res.append(elem)
        else:
            res.append(str(elem))
    return " ".join(res)


def extract(text: str) -> List[Citation]:
    """
    This function extracts and returns a list of citations.

    :param text: The text on which to work.
    :return: A list of the extracted citations.
    :rtype: List[Citation]
    """
    processed = process(text)
    return list(filter(lambda x: isinstance(x, Citation), processed))
