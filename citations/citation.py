from dataclasses import dataclass
from typing import Optional, List, Union

citation_parts = ["paragraph", "absatz", "satz", "buchstabe", "gesetz"]


@dataclass
class Citation:
    paragraph: Optional[int]
    absatz: Optional[int]
    satz: Optional[int]
    buchstabe: Optional[str]
    gesetz: Optional[str]
    text: str
    context: str

    @classmethod
    def new(cls, context: List[str]):
        return Citation(None, None, None, None, None, "", " ".join(context))

    @classmethod
    def from_tuple(cls, data):
        """
        Creates an new Citation from a tuple of citation
        parts. The context and text are left blank.
        :param data: A tuple of the citation data with exactly 5 elements
        :return: A new citation, or None if the tuple is bad.
        """
        if len(data) != 5:
            return None
        cit = Citation.new([])
        for i in range(5):
            cit[citation_parts[i]] = data[i]

    def set_value(
        self, key: str, value: Union[int, str], overwrite: bool = False
    ) -> bool:
        """
        This function sets a citation element to the given value.

        :param key: The parameter name.
        :param value: The value.
        :param overwrite: Whether an existing element should be overwritten.
            If False, setting an existing attribute returns False.
        :return: Whether the attribute is allowed to be set.
        """
        if key not in [
            "paragraph",
            "absatz",
            "satz",
            "buchstabe",
            "gesetz",
            "text",
            "context",
        ]:
            return False
        if getattr(self, key) is not None and not overwrite:
            return False
        setattr(self, key, value)
        return True

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, key, value):
        return self.set_value(key, value, overwrite=False)

    def __eq__(self, other) -> bool:
        """
        I check only the five citation elements for equality.
        """
        if not isinstance(other, Citation):
            return False
        for key in citation_parts:
            if self[key] != other[key]:
                return False
        return True

    def __contains__(self, item) -> bool:
        """
        A citation contains another citation,
        if the second is the same place as the first, but finer.
        Therefore, I check that for each not None element of the first,
        the second has the same.
        Thus, I return False if either left has something, while right has nothing,
        or the values are unequal.
        """
        if not isinstance(item, Citation):
            return False
        for key in citation_parts:
            if self[key] is not None and item[key] is not None:
                return False
            if self[key] != item[key]:
                return False
        return True

    def is_valid(self) -> bool:
        if self.paragraph is None or self.gesetz is None:
            return False
        return True

    def __str__(self) -> str:
        if not self.is_valid():
            return "Invalid Citation"
        res = f"Artikel {self.paragraph}"
        if self.absatz is not None:
            res += f" Absatz {self.absatz}"
        if self.satz is not None:
            res += f" Satz {self.satz}"
        if self.buchstabe is not None:
            res += f" Buchstabe {self.buchstabe}"
        return f"{res} {self.gesetz}"
