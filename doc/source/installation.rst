Installation
============

The stable version can be found on the pypi testing repositories: `test.pypi.org <https://test.pypi.org/project/citations>`_
The latest (and potentially unstable version) can be found on `Gitlab <https://lion-sz.gitlab.io/citation-detection/citations-latest.whl>`_.