Citation Detection
==================

A python package to extract legal citations.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

    Installation <installation.rst>

    Usage <usage.rst>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
