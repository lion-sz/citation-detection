Usage
=====

Interface
#########

The package provides two main functionalities, extracting a list of citation objects from a string and replacing all citations in a string with a standardized format.
To do this there are three functions:

.. automodule:: citations.interface
    :members: replace, replace_html, extract


Keywords
########

The keywords used by the citation extraction can be altered and updated by the users.
To do so the package exports a few functions to the user.

.. automodule:: citations.keywords
    :members: get_keywords, set_keywords